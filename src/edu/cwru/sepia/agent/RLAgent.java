package edu.cwru.sepia.agent;

import edu.cwru.sepia.action.Action;
import edu.cwru.sepia.action.ActionFeedback;
import edu.cwru.sepia.action.ActionResult;
import edu.cwru.sepia.action.TargetedAction;
import edu.cwru.sepia.environment.model.history.DamageLog;
import edu.cwru.sepia.environment.model.history.DeathLog;
import edu.cwru.sepia.environment.model.history.History;
import edu.cwru.sepia.environment.model.state.State;
import edu.cwru.sepia.environment.model.state.Unit;

import java.io.*;
import java.util.*;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.stream.*;

public class RLAgent extends Agent {

     public boolean evaluationEpisode = false;
     public int episodeNum = 1;
     public int episodeNumNoEval = 1;
     public boolean firstTurn = true;
     public double currentEpReward = 0.0;
     public List<Double> avgCumulRewards;
     public List<Double> evaluationSeriesRewards;

     public Map<Integer, Integer> lastAttacks;

     /**
      * Set in the constructor. Defines how many learning episodes your agent should run for.
      * When starting an episode. If the count is greater than this value print a message
      * and call sys.exit(0)
      */
     public final int numEpisodes;

     /**
      * List of your footmen and your enemies footmen
      */
     private List<Integer> myFootmen;
     private List<Integer> enemyFootmen;

     /**
      * Convenience variable specifying enemy agent number. Use this whenever referring
      * to the enemy agent. We will make sure it is set to the proper number when testing your code.
      */
     public static final int ENEMY_PLAYERNUM = 1;

     /**
      * Set this to whatever size your feature vector is.
      */
     public static final int NUM_FEATURES = 6;

     /** Use this random number generator for your epsilon exploration. When you submit we will
      * change this seed so make sure that your agent works for more than the default seed.
      */
     public final Random random = new Random(12345);

     /**
      * Your Q-function weights.
      */
     public Double[] weights;

     /**
      * These variables are set for you according to the assignment definition. You can change them,
      * but it is not recommended. If you do change them please let us know and explain your reasoning for
      * changing them.
      */
     public final double gamma = 0.9;
     public final double learningRate = .0001;
     public final double epsilon = .02;

     public RLAgent(int playernum, String[] args) {
          super(playernum);

          avgCumulRewards = new ArrayList<>();
          evaluationSeriesRewards = new ArrayList<>();
          lastAttacks = new HashMap<>();

          if (args.length >= 1) {
               numEpisodes = Integer.parseInt(args[0]);
               System.out.println("Running " + numEpisodes + " episodes.");
          } else {
               numEpisodes = 10;
               System.out.println("Warning! Number of episodes not specified. Defaulting to 10 episodes.");
          }

          boolean loadWeights = false;
          if (args.length >= 2) {
               loadWeights = Boolean.parseBoolean(args[1]);
          } else {
               System.out.println("Warning! Load weights argument not specified. Defaulting to not loading.");
          }

          if (loadWeights) {
               weights = loadWeights();
          } else {
               // initialize weights to random values between -1 and 1
               weights = new Double[NUM_FEATURES];
               for (int i = 0; i < weights.length; i++) {
                    weights[i] = random.nextDouble() * 2 - 1;
               }
          }
     }

     /**
      * We've implemented some setup code for your convenience. Change what you need to.
      */
     @Override
     public Map<Integer, Action> initialStep(State.StateView stateView, History.HistoryView historyView) {

          currentEpReward = 0.0;
          evaluationEpisode = (episodeNum - 1) % 15 > 9; // ten games regular, 5 eval, etc
          firstTurn = true;
          lastAttacks.clear();

          // Find all of your units
          myFootmen = new LinkedList<>();
          for (Integer unitId : stateView.getUnitIds(playernum)) {
               Unit.UnitView unit = stateView.getUnit(unitId);

               String unitName = unit.getTemplateView().getName().toLowerCase();
               if (unitName.equals("footman")) {
                    myFootmen.add(unitId);
               } else {
                    System.err.println("Unknown unit type: " + unitName);
               }
          }

          // Find all of the enemy units
          enemyFootmen = new LinkedList<>();
          for (Integer unitId : stateView.getUnitIds(ENEMY_PLAYERNUM)) {
               Unit.UnitView unit = stateView.getUnit(unitId);

               String unitName = unit.getTemplateView().getName().toLowerCase();
               if (unitName.equals("footman")) {
                    enemyFootmen.add(unitId);
               } else {
                    System.err.println("Unknown unit type: " + unitName);
               }
          }

          return middleStep(stateView, historyView);
     }

     /**
      * You will need to calculate the reward at each step and update your totals. You will also need to
      * check if an event has occurred. If it has then you will need to update your weights and select a new action.
      *
      * If you are using the footmen vectors you will also need to remove killed units. To do so use the historyView
      * to get a DeathLog. Each DeathLog tells you which player's unit died and the unit ID of the dead unit. To get
      * the deaths from the last turn do something similar to the following snippet. Please be aware that on the first
      * turn you should not call this as you will get nothing back.
      *
      * for(DeathLog deathLog : historyView.getDeathLogs(stateView.getTurnNumber() -1)) {
      *     System.out.println("Player: " + deathLog.getController() + " unit: " + deathLog.getDeadUnitID());
      * }
      *
      * You should also check for completed actions using the history view. Obviously you never want a footman just
      * sitting around doing nothing (the enemy certainly isn't going to stop attacking). So at the minimum you will
      * have an even whenever one your footmen's targets is killed or an action fails. Actions may fail if the target
      * is surrounded or the unit cannot find a path to the unit. To get the action results from the previous turn
      * you can do something similar to the following. Please be aware that on the first turn you should not call this
      *
      * Map<Integer, ActionResult> actionResults = historyView.getCommandFeedback(playernum, stateView.getTurnNumber() - 1);
      * for(ActionResult result : actionResults.values()) {
      *     System.out.println(result.toString());
      * }
      *
      * @return New actions to execute or nothing if an event has not occurred.
      */
     @Override
     public Map<Integer, Action> middleStep(State.StateView stateView, History.HistoryView historyView) {
          Map<Integer, Action> res = new HashMap<>();

          if(firstTurn) {
               firstTurn = false;
               // choose actions for each foorman on turn 1
               for(Integer attackerId : myFootmen) {
                    int targetId = selectAction(stateView, historyView, attackerId);
                    res.put(attackerId, TargetedAction.createCompoundAttack(attackerId, targetId));
                    lastAttacks.put(attackerId, targetId); // save these attacks for the next time we update weights etc
               }
          }
          else {
               boolean somethingHappened = false;
               // check if an action failed
               Map<Integer, ActionResult> actionResults = historyView.getCommandFeedback(playernum, stateView.getTurnNumber() - 1);
               for(ActionResult result : actionResults.values()) {
                    if(result.getFeedback() == ActionFeedback.FAILED) {
                         somethingHappened = true;
                         break;
                    }
               }

               // remove dead units and check if a unit died
               for(DeathLog deathLog : historyView.getDeathLogs(stateView.getTurnNumber() -1)) {
                    // if any unit dies, trigger a "something happened event"
                    somethingHappened = true;
                    if(deathLog.getController() == playernum) {
                         myFootmen.remove((Object)deathLog.getDeadUnitID());
                    }
                    else {
                         enemyFootmen.remove((Object)deathLog.getDeadUnitID());
                    }
               }

               if(!somethingHappened) {
                    return res; // do nothing
               }

               // calculate the reward at each step and update totals
               // update weights (if not in testing episode) and select a new action.
               for(Integer footmanId : myFootmen) {
                    double reward = calculateReward(stateView, historyView, footmanId);
                    currentEpReward += reward;

                    if(!evaluationEpisode) {
                         double totalReward;
                         weights = updateWeights(weights,
                                                 calculateFeatureVector(stateView, historyView, footmanId, lastAttacks.get(footmanId)),
                                                 reward, stateView, historyView, footmanId);
                    }
               }

               // choose actions for each foorman if something happened
               for(Integer attackerId : myFootmen) {
                    int targetId = selectAction(stateView, historyView, attackerId);
                    res.put(attackerId, TargetedAction.createCompoundAttack(attackerId, targetId));
                    lastAttacks.put(attackerId, targetId); // save these attacks for the next time we update weights etc
               }

          }

          return res;
     }


     /**
      * Here you will calculate the cumulative average rewards for your testing episodes. If you have just
      * finished a set of test episodes you will call out testEpisode.
      *
      * It is also a good idea to save your weights with the saveWeights function.
      */
     @Override
     public void terminalStep(State.StateView stateView, History.HistoryView historyView) {
          // update unit lists
          for(DeathLog deathLog : historyView.getDeathLogs(stateView.getTurnNumber() -1)) {
               if(deathLog.getController() == playernum) {
                    myFootmen.remove((Object)deathLog.getDeadUnitID());
               }
               else {
                    enemyFootmen.remove((Object)deathLog.getDeadUnitID());
               }
          }
          // if evaluating, add cumul reward to avg rewards

          // if ep# -1 % 15 == 10 (first eval game), blank avg reward list first
          if((episodeNum - 1) % 15 == 10) {
               evaluationSeriesRewards.clear();
               System.out.println("STARTING EVALS");
          }

          if(evaluationEpisode) {
               evaluationSeriesRewards.add(currentEpReward);
               currentEpReward = 0.0;
          }
          else {
               episodeNumNoEval++;
          }

          // last eval ep in series of 5
          // compute avg cumul reward, add to list of them, blank out seriesrewards list
          if((episodeNum - 1) % 15 == 14) {
               double avg = 0.0;
               for(Double d : evaluationSeriesRewards) {
                    avg += d;
               }
               avg = avg / evaluationSeriesRewards.size();
               avgCumulRewards.add(avg);
               System.out.println("******avg cumul reward for eval series " + avg);

          }

          if(myFootmen.size() > 0) {
               System.out.println("we won!");
          } else {
               System.out.println("we lost...");
          }

          // Save your weights
          saveWeights(weights);

          if(episodeNumNoEval >= numEpisodes) {
               System.out.println("did all episodes, exiting");
               // print avg cumulative rewards
               printTestData(avgCumulRewards);
               System.exit(0);
          }

          // last!
          lastAttacks.clear();
          episodeNum++;
     }

     /**
      * Calculate the updated weights for this agent.
      * @param oldWeights Weights prior to update
      * @param oldFeatures Features from (s,a)
      * @param totalReward Cumulative discounted reward for this footman.
      * @param stateView Current state of the game.
      * @param historyView History of the game up until this point
      * @param footmanId The footman we are updating the weights for
      * @return The updated weight vector.
      */
     public Double[] updateWeights(Double[] oldWeights, double[] oldFeatures, double totalReward, State.StateView stateView, History.HistoryView historyView, int footmanId) {

          Double[] newWeights = new Double[NUM_FEATURES];

          // find Q(s', a')
          int targetId = selectAction(stateView, historyView, footmanId);
          double qNew = calcQValue(stateView, historyView, footmanId, targetId);
          // Q(s, a)
          double qOld = calcQValue(stateView, historyView, footmanId, lastAttacks.get(footmanId));

          // update weights vector for this agent
          //  for each feature fi, do:
          //     w_i = w_i + alpha * (reward + gamma*Qnew - Qprev)
          // from lec 23, slide 30

          for(int i = 0; i < NUM_FEATURES; i++) {
               newWeights[i] = oldWeights[i] + learningRate * (totalReward + gamma * qNew - qOld) * oldFeatures[i];
          }

          return newWeights;
     }

     /**
      * Given a footman and the current state and history of the game select the enemy that this unit should
      * attack. This is where you would do the epsilon-greedy action selection.
      *
      * @param stateView Current state of the game
      * @param historyView The entire history of this episode
      * @param attackerId The footman that will be attacking
      * @return The enemy footman ID this unit should attack
      */
     public int selectAction(State.StateView stateView, History.HistoryView historyView, int attackerId) {
          int targetId = -1;

          // if not evaluating strat, pick random target with probability epsilon
          if(!evaluationEpisode && random.nextDouble() <= epsilon) {
               // attack random enemy
               targetId = enemyFootmen.get(random.nextInt(enemyFootmen.size()));
          }
          // else, get the target giving the highest Q(s,a)
          else {
               double qMax = - Double.MAX_VALUE;
               targetId = enemyFootmen.get(0); // fallback

               for(Integer enemy : enemyFootmen) {
                    double q = calcQValue(stateView, historyView, attackerId, enemy);
                    if(q > qMax) {
                         qMax = q;
                         targetId = enemy;
                    }
               }
          }

          return targetId;
     }

     /**
      * Given the current state and the footman in question calculate the reward received on the last turn.
      * This is where you will check for things like Did this footman take or give damage? Did this footman die
      * or kill its enemy. Did this footman start an action on the last turn? See the assignment description
      * for the full list of rewards.
      *
      * Remember that you will need to discount this reward based on the timestep it is received on. See
      * the assignment description for more details.
      *
      * As part of the reward you will need to calculate if any of the units have taken damage. You can use
      * the history view to get a list of damages dealt in the previous turn. Use something like the following.
      *
      * for(DamageLog damageLogs : historyView.getDamageLogs(lastTurnNumber)) {
      *     System.out.println("Defending player: " + damageLog.getDefenderController() + " defending unit: " + \
      *     damageLog.getDefenderID() + " attacking player: " + damageLog.getAttackerController() + \
      *     "attacking unit: " + damageLog.getAttackerID());
      * }
      *
      * You will do something similar for the deaths. See the middle step documentation for a snippet
      * showing how to use the deathLogs.
      *
      * To see if a command was issued you can check the commands issued log.
      *
      * Map<Integer, Action> commandsIssued = historyView.getCommandsIssued(playernum, lastTurnNumber);
      * for (Map.Entry<Integer, Action> commandEntry : commandsIssued.entrySet()) {
      *     System.out.println("Unit " + commandEntry.getKey() + " was command to " + commandEntry.getValue().toString);
      * }
      *
      * @param stateView The current state of the game.
      * @param historyView History of the episode up until this turn.
      * @param footmanId The footman ID you are looking for the reward from.
      * @return The current reward
      */
     public double calculateReward(State.StateView stateView, History.HistoryView historyView, int footmanId) {
          // discounted by multiplying with gamma ^ (turnNum -1)

          // Each action costs the agent -0.1.
          double reward = -0.1;

          int turnNum = stateView.getTurnNumber();

          // If a friendly footman hits an enemy for d damage, the agent gets a reward of +d.
          // If a friendly footman gets hit for d damage, the agent gets a  penalty  of  –d.
          for(DamageLog damageLog : historyView.getDamageLogs(turnNum - 1)) {
               if(damageLog.getDefenderController() == playernum) {
                    reward -= damageLog.getDamage();
               }
               else {
                    reward += damageLog.getDamage();
               }
          }

          //  If  an  enemy  footman  dies, the agent  gets  a  reward  of  +100. If  a  friendly footman dies, the agent gets a penalty of -100.
          for(DeathLog deathLog : historyView.getDeathLogs(turnNum - 1)) {
               if(deathLog.getController() == playernum) {
                    reward -= 100;
               }
               else {
                    reward += 100;
               }
          }

          reward *= Math.pow(gamma, turnNum - 1);

          return reward;
     }

     /**
      * This is where you will calculate
      * your features and multiply them by your current weights to get the approximate Q-value.
      *
      * Calculate the Q-Value for a given state action pair. The state in this scenario is the current
      * state view and the history of this episode. The action is the attacker and the enemy pair for the
      * SEPIA attack action.
      *
      * This returns the Q-value according to your feature approximation. This is where you will calculate
      * your features and multiply them by your current weights to get the approximate Q-value.
      *
      * @param stateView Current SEPIA state
      * @param historyView Episode history up to this point in the game
      * @param attackerId Your footman. The one doing the attacking.
      * @param defenderId An enemy footman that your footman would be attacking
      * @return The approximate Q-value
      */
     public double calcQValue(State.StateView stateView,
                              History.HistoryView historyView,
                              int attackerId,
                              int defenderId) {

          double[] fs = calculateFeatureVector(stateView, historyView, attackerId, defenderId);
          double q = weights[0]; // first const

          for(int i = 0; i < NUM_FEATURES; i++) {
               q += fs[i] * weights[i];
          }

          return q;
     }

     /**
      * Given a state and action calculate your features here. Please include a comment explaining what features
      * you chose and why you chose them.
      *
      * All of your feature functions should evaluate to a double. Collect all of these into an array. You will
      * take a dot product of this array with the weights array to get a Q-value for a given state action.
      *
      * It is a good idea to make the first value in your array a constant. This just helps remove any offset
      * from 0 in the Q-function. The other features are up to you. Many are suggested in the assignment
      * description.
      *
      * @param stateView Current state of the SEPIA game
      * @param historyView History of the game up until this turn
      * @param attackerId Your footman. The one doing the attacking.
      * @param defenderId An enemy footman. The one you are considering attacking.
      * @return The array of feature function outputs.
      */
     public double[] calculateFeatureVector(State.StateView stateView,
                                            History.HistoryView historyView,
                                            int attackerId,
                                            int defenderId) {

          double[] features = new double[NUM_FEATURES];

          // const
          features[0] = 4.2;

          // health of footman
          features[1] = stateView.getUnit(attackerId).getHP();

          // health of target
          // if the target died last turn and we're checking those attacks, their health = 0 ??
          if(stateView.getUnit(defenderId) == null) {
               features[2] = 0;
          }
          else {
               features[2] = stateView.getUnit(defenderId).getHP();
          }

          // number of our footmen attacking this target
          features[3] = lastAttacks.values()
               .stream()
               .filter( e -> e == defenderId)
               .count();

          // is e my closest enemy?
          Integer atkX = stateView.getUnit(attackerId).getXPosition();
          Integer atkY = stateView.getUnit(attackerId).getYPosition();

          Integer currentClosest = -1;
          Integer currentClosestX = -1;
          Integer currentClosestY = -1;
          Integer closestDist = Integer.MAX_VALUE;

          for(Integer enemy : enemyFootmen) {
               if(currentClosest == -1) {
                    currentClosest = enemy;
                    currentClosestX = stateView.getUnit(enemy).getXPosition();
                    currentClosestY = stateView.getUnit(enemy).getYPosition();
               }
               if(chebyshev(atkX, atkY, stateView.getUnit(enemy).getXPosition(), stateView.getUnit(enemy).getYPosition()) < closestDist) {
                    closestDist = chebyshev(atkX, atkY, stateView.getUnit(enemy).getXPosition(), stateView.getUnit(enemy).getYPosition());
                    currentClosest = enemy;
                    currentClosestX = stateView.getUnit(enemy).getXPosition();
                    currentClosestY = stateView.getUnit(enemy).getYPosition();
               }
          }

          if(currentClosest == -1 || currentClosest != defenderId) {
               features[4] = 0;
          }
          else {
               features[4] = 5;
          }

          // is enemy adjacent?
          if(stateView.getUnit(defenderId) != null && stateView.getUnit(attackerId) != null) {
               features[5] = (Math.abs(stateView.getUnit(defenderId).getXPosition()- stateView.getUnit(attackerId).getXPosition()) <= 1 &&
                              Math.abs(stateView.getUnit(defenderId).getYPosition()- stateView.getUnit(attackerId).getYPosition()) <= 1) ? 10 : -10;
          } else {
               features[5] = 0;
          }

          return features;
     }

     /**
      * DO NOT CHANGE THIS!
      *
      * Prints the learning rate data described in the assignment. Do not modify this method.
      *
      * @param averageRewards List of cumulative average rewards from test episodes.
      */
     public void printTestData (List<Double> averageRewards) {
          System.out.println("");
          System.out.println("Games Played      Average Cumulative Reward");
          System.out.println("-------------     -------------------------");
          for (int i = 0; i < averageRewards.size(); i++) {
               String gamesPlayed = Integer.toString(10*i);
               String averageReward = String.format("%.2f", averageRewards.get(i));

               int numSpaces = "-------------     ".length() - gamesPlayed.length();
               StringBuffer spaceBuffer = new StringBuffer(numSpaces);
               for (int j = 0; j < numSpaces; j++) {
                    spaceBuffer.append(" ");
               }
               System.out.println(gamesPlayed + spaceBuffer.toString() + averageReward);
          }
          System.out.println("");
     }

     /**
      * DO NOT CHANGE THIS!
      *
      * This function will take your set of weights and save them to a file. Overwriting whatever file is
      * currently there. You will use this when training your agents. You will include th output of this function
      * from your trained agent with your submission.
      *
      * Look in the agent_weights folder for the output.
      *
      * @param weights Array of weights
      */
     public void saveWeights(Double[] weights) {
          File path = new File("agent_weights/weights.txt");
          // create the directories if they do not already exist
          path.getAbsoluteFile().getParentFile().mkdirs();

          try {
               // open a new file writer. Set append to false
               BufferedWriter writer = new BufferedWriter(new FileWriter(path, false));

               for (double weight : weights) {
                    writer.write(String.format("%f\n", weight));
               }
               writer.flush();
               writer.close();
          } catch(IOException ex) {
               System.err.println("Failed to write weights to file. Reason: " + ex.getMessage());
          }
     }

     /**
      * DO NOT CHANGE THIS!
      *
      * This function will load the weights stored at agent_weights/weights.txt. The contents of this file
      * can be created using the saveWeights function. You will use this function if the load weights argument
      * of the agent is set to 1.
      *
      * @return The array of weights
      */
     public Double[] loadWeights() {
          File path = new File("agent_weights/weights.txt");
          if (!path.exists()) {
               System.err.println("Failed to load weights. File does not exist");
               return null;
          }

          try {
               BufferedReader reader = new BufferedReader(new FileReader(path));
               String line;
               List<Double> weights = new LinkedList<>();
               while((line = reader.readLine()) != null) {
                    weights.add(Double.parseDouble(line));
               }
               reader.close();

               return weights.toArray(new Double[weights.size()]);
          } catch(IOException ex) {
               System.err.println("Failed to load weights from file. Reason: " + ex.getMessage());
          }
          return null;
     }

     @Override
     public void savePlayerData(OutputStream outputStream) {

     }

     @Override
     public void loadPlayerData(InputStream inputStream) {

     }

     private int chebyshev(int xa, int ya, int xb, int yb) {
          return Math.max(Math.abs(xb-xa), Math.abs(yb-ya));
     }

}
